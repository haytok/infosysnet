from django import forms


class FlagForm(forms.Form):
    name = forms.CharField(label='Name')
    password = forms.CharField(label='Password')
