from django.views.generic import TemplateView
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from .forms import FlagForm
from infosysnet import settings


class TopView(TemplateView):
    template_name = 'index/top.html'


class IndexView(TemplateView):
    template_name = 'index/index.html'


class Remark1View(TemplateView):
    template_name = 'index/remark1.html'


class Remark2View(TemplateView):
    template_name = 'index/remark2.html'


class Remark3View(TemplateView):
    template_name = 'index/remark3.html'


class ForbiddenClass(TemplateView):
    template_name = 'index/forbidden.html'


class FlakeFlagClass(TemplateView):
    template_name = 'index/fakeflag.html'


class FlagClass(TemplateView):
    form_class = FlagForm
    template_name = 'index/flag.html'

    def __init__(self, **kwargs):
        self.params = {
            'form': FlagForm()
        }

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(FlagClass, self).dispatch(*args, **kwargs)

    def get(self, request):
        return render(request, 'index/flag.html', self.params)

    def post(self, request):
        name = request.POST['name']
        password = request.POST['password']

        if name == settings.NAME and password == settings.PASSWORD:
            return render(request, 'index/flag2.html')

        return render(request, 'index/flag.html', self.params)
